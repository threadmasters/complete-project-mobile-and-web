require('dotenv').config();

const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const AdminUsersRoute = require('./routes/AdminUsersRoute');
const CreatePostroute = require('./routes/CreatePostroute');
const session = require('express-session'); // Import express-session

const app = express();
const secretKey = 'MAMAMO';

app.use(express.json());
app.use((req, res, next) => {
  console.log(req.path, req.method);
  next();
});

app.use(cors({
  origin: 'http://localhost:3000',
  credentials: true,
}));

// Set up the express-session middleware
app.use(
  session({
    secret: secretKey, // Replace with your secret key
    resave: false,
    saveUninitialized: true,
  })
);

mongoose.connect(process.env.MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => {
    console.log('Connected to MongoDB Atlas');
  })
  .catch((error) => {
    console.error('Error connecting to MongoDB:', error);
  });

app.use('/uploads', express.static('C:\\Users\\Lesley\\Desktop\\checking\\UPANG__BULLETIN_MAIN\\storage'));
app.use('/Admin', AdminUsersRoute);
app.use('/Post', CreatePostroute);

app.listen(process.env.PORT, () => {
  console.log('Listening on port', process.env.PORT);
});
