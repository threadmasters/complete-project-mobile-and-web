const mongoose = require('mongoose');

const AdminUsersSchema = new mongoose.Schema({
  
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  isAdmin: {
    type: Boolean,
    default: true, 
  },
});

const AdminUsers = mongoose.model('AdminUsers', AdminUsersSchema);
module.exports = AdminUsers;