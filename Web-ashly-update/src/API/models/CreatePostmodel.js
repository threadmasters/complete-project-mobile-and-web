const mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
  title: String,
  text: String,
  images: [String], 
  metaDescription: String,
  slugTitle: String,
  featured: Boolean,
});

const Post = mongoose.model('Post', postSchema);

module.exports = Post;