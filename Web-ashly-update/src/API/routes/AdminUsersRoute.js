const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt'); 
const jwt = require('jsonwebtoken'); 


const AdminUsers = require('../models/AdminUsers');

router.post('/register', async (req, res) => {
  try {
    const { email, password, isAdmin } = req.body; 
    const userExists = await AdminUsers.findOne({ email });

    if (userExists) {
      return res.status(400).json({ message: 'User already exists' });
    }

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    const newUser = new AdminUsers({
      email,
      password: hashedPassword,
      isAdmin, 
    });

    await newUser.save();
    return res.status(201).json({ message: 'User registered successfully' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Server error' });
  }
});



router.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await AdminUsers.findOne({ email });

    if (!user) {
      return res.status(401).json({ message: 'User not found' });
    }

    console.log('User found:', user); 

    const validPassword = await bcrypt.compare(password, user.password);

    if (!validPassword) {
      return res.status(401).json({ message: 'Invalid password' });
    }

    console.log('isAdmin:', user.isAdmin); 

    if (!user.isAdmin) {
      return res.status(403).json({ message: 'Permission denied. Not an admin.' });
    }

    
    const token = jwt.sign({ _id: user._id }, process.env.JWT);

    res.json({ token });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Server error' });
  }
});


router.post('/logout', (req, res) => {

 
  req.session.destroy((err) => {
    if (err) {
      console.error(err);
      return res.status(500).json({ message: 'Server error during logout' });
    }
    res.status(200).json({ message: 'Logged out successfully' });
  });
});

module.exports = router;