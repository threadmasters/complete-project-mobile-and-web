import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const Home = () => {
  const [posts, setPosts] = useState([]);
  const [editPost, setEditPost] = useState(null);
  const [updateData, setUpdateData] = useState({
    title: '',
    text: '',
    metaDescription: '',
    slugTitle: '',
    featured: false, 
  });

  useEffect(() => {
   
    fetch('http://localhost:3600/Post/all')
      .then((response) => response.json())
      .then((data) => setPosts(data))
      .catch((error) => console.error('Error fetching data:', error));
  }, []);

  const handleUpdate = (post) => {
    
    setEditPost(post);

    
    setUpdateData({
      title: post.title,
      text: post.text,
      metaDescription: post.metaDescription,
      slugTitle: post.slugTitle,
      featured: post.featured, 
    });
  };

  const handleCancelEdit = () => {
    
    setEditPost(null);
    setUpdateData({
      title: '',
      text: '',
      metaDescription: '',
      slugTitle: '',
      featured: false, 
    });
  };

  const handleUpdateSubmit = () => {
    
    fetch(`http://localhost:3600/Post/${editPost._id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(updateData),
    })
      .then((response) => {
        if (response.status === 200) {
          
          const updatedPosts = posts.map((post) =>
            post._id === editPost._id ? { ...post, ...updateData } : post
          );
          setPosts(updatedPosts);
          
          handleCancelEdit();
        } else {
          console.error('Error updating the post');
        }
      })
      .catch((error) => console.error('Error updating the post:', error));
  };

  const handleDelete = (id) => {
    if (window.confirm('Are you sure you want to delete this post?')) {
      
      fetch(`http://localhost:3600/Post/${id}`, {
        method: 'DELETE',
      })
        .then((response) => {
          if (response.status === 200) {
            
            setPosts((prevPosts) => prevPosts.filter((post) => post._id !== id));
          } else {
            console.error('Error deleting the post');
          }
        })
        .catch((error) => console.error('Error deleting the post:', error));
    }
  };

  return (
    <div>
      <nav className="sticky top-0 z-50 font-sans flex flex-col text-center sm:flex-row sm:text-left sm:justify-between py-4 px-6 bg-white shadow sm:items-baseline w-full">
        <div className="mb-2 sm:mb-0">
          <a href="#" className="text-2xl no-underline text-grey-darkest hover:text-blue-dark">
            UPang Bulletin
          </a>
        </div>
        <div>
          <Link to="/login">
            <a href="#" className="text-lg no-underline text-grey-darkest hover:text-blue-dark ml-2">
              Login
            </a>
          </Link>
        </div>
      </nav>

      <div className="container mx-auto p-4">
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
          {posts.map((post) => (
            <div key={post._id} className="bg-white shadow-md rounded-lg p-4 relative" style={{ width: '325px', maxHeight: '550px', overflowY: 'auto' }}>
              <img
                src={`http://localhost:3600/uploads/${post.images[0]}`}
                alt={post.title}
                className="w-32 h-32 mb-4 rounded-full mx-auto"
              />
              <h2 className="text-2xl font-bold">{post.title}</h2>
              <div style={{ maxHeight: '150px', overflowY: 'auto' }}>
                <p className="text-gray-700 text-lg">{post.text}</p>
              </div>
              <p className="text-gray-700">{post.metaDescription}</p>
              <p className="text-gray-700">{post.slugTitle}</p>
              {editPost && editPost._id === post._id ? (
                <div>
                  <div className="mb-3">
                    <label htmlFor="title" className="text-gray-700">Title:</label>
                    <input
                      type="text"
                      id="title"
                      value={updateData.title}
                      onChange={(e) => setUpdateData({ ...updateData, title: e.target.value })}
                      className="w-full border rounded px-2 py-1"
                    />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="text" className="text-gray-700">Text:</label>
                    <textarea
                      id="text"
                      value={updateData.text}
                      onChange={(e) => setUpdateData({ ...updateData, text: e.target.value })}
                      className="w-full border rounded px-2 py-1"
                    />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="metaDescription" className="text-gray-700">Meta Description:</label>
                    <input
                      type="text"
                      id="metaDescription"
                      value={updateData.metaDescription}
                      onChange={(e) => setUpdateData({ ...updateData, metaDescription: e.target.value })}
                      className="w-full border rounded px-2 py-1"
                    />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="slugTitle" className="text-gray-700">Slug Title:</label>
                    <input
                      type="text"
                      id="slugTitle"
                      value={updateData.slugTitle}
                      onChange={(e) => setUpdateData({ ...updateData, slugTitle: e.target.value })}
                      className="w-full border rounded px-2 py-1"
                    />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="featured" className="text-gray-700">Featured:</label>
                    <input
                      type="checkbox"
                      id="featured"
                      checked={updateData.featured}
                      onChange={(e) => setUpdateData({ ...updateData, featured: e.target.checked })}
                      className="w-full"
                    />
                  </div>
                  
                  <button onClick={handleCancelEdit} className="bg-gray-400 hover.bg-gray-600 text-white font-bold py-2 px-4 rounded focus-outline-none focus-shadow-outline m-2">
                    Cancel
                  </button>
                  <button onClick={handleUpdateSubmit} className="bg-blue-500 hover.bg-blue-700 text-white font-bold py-2 px-4 rounded focus-outline-none focus-shadow-outline m-2">
                    Save
                  </button>
                </div>
              ) : (
                <div>
                  <button onClick={() => handleUpdate(post)} className="bg-blue-500 hover.bg-blue-700 text-white font-bold py-2 px-4 rounded focus-outline-none focus-shadow-outline m-2">
                    Update
                  </button>
                  <button onClick={() => handleDelete(post._id)} className="bg-red-500 hover.bg-red-700 text-white font-bold py-2 px-4 rounded focus-outline-none focus-shadow-outline m-2">
                    Delete
                  </button>
                </div>
              )}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;