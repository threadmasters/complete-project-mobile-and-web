import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const handleLogout = async () => {
  try {
    const response = await axios.post('http://localhost:3600/Admin/logout');
    if (response.status === 200) {
      alert('You are now logged out!');
      console.log('Logout successful.'); 
    } else {
      alert('Logout failed. Please try again.');
      console.error('Logout failed. Please try again.'); 
    }
  } catch (error) {
    console.error('Logout error:', error);
    alert('Logout failed. Please try again.');
  }
};

function Settings() {
  return (
    <div>
      <div>Settings</div>
      <div>
        <button
          className="my-20 bg-green-900 text-white py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          onClick={handleLogout}
        >
          Log Out
        </button>
      </div>
      <p>
        <Link to="/login" className="text-yellow-400">
          Return to Login
        </Link>
      </p>
    </div>
  );
}

export default Settings;