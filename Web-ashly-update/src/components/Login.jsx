import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const [successMessage, setSuccessMessage] = useState(null);
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError(null);
    setSuccessMessage(null);
  
    try {
      const response = await axios.post('http://localhost:3600/Admin/login', {
        email,
        password,
      });
  
      if (response.status === 200) {
        
        const token = response.data.token;
        localStorage.setItem('token', token);
  
        
        setSuccessMessage('Login successful!');
  
        
        alert('Login successful!');
  
        
        console.log('Login successful!');
  
        
        navigate('/');
      } else {
        setError('Login failed');
        alert('Login failed');
        console.log('Login failed');
      }
    } catch (error) {
      setError('Login failed');
      alert('Login failed');
      console.error('Login failed:', error);
    }
  };
  

  return (
    <div className="bg-green-900 p-8 rounded-3xl shadow-lg">
      <h2 className="text-2xl font-bold mb-4 text-white">Login</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2">Email:</label>
          <input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
            className="w-full px-3 py-2 border border-gray-300 rounded"
          />
        </div>
        <div className="mb-6">
          <label className="block text-white text-sm font-bold mb-2">Password:</label>
          <input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
            className="w-full px-3 py-2 border border-gray-300 rounded"
          />
        </div>
        {error && <p className="text-red-500 mb-4">{error}</p>}
        {successMessage && <p className="text-green-500 mb-4">{successMessage}</p>}
        <button
          type="submit"
          className="bg-yellow-400 text-black py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Login
        </button>
      </form>
      <p className="mt-4 text-white">
        Don't have an account? <Link to="/signup" className="text-yellow-400">Sign Up</Link>
      </p>
    </div>
  );
};

export default Login;