import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

const SignUp = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const [signupSuccess, setSignupSuccess] = useState(false);
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError(null);
  
    try {
      const response = await fetch('http://localhost:3600/Admin/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      });
  
      if (response.status === 201) {
       
        setSignupSuccess(true);
  
        
        alert('Sign-up successful!');
        console.log('Sign-up successful!');
  
        
        navigate('/login');
      } else {
        const data = await response.json();
        setError(data.message);
        alert('Sign-up failed');
        console.log('Sign-up failed:', data.message);
      }
    } catch (error) {
      setError('Server error');
      alert('Server error');
      console.error('Server error:', error);
    }
  };

  return (
    <div className="bg-green-900 p-8 rounded-3xl shadow-lg">
      <h2 className="text-2xl font-bold mb-4 text-white">Sign Up</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2">Email:</label>
          <input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
            className="w-full px-3 py-2 border border-gray-300 rounded"
          />
        </div>
        <div className="mb-6">
          <label className="block text-white text-sm font-bold mb-2">Password:</label>
          <input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
            className="w-full px-3 py-2 border border-gray-300 rounded"
          />
        </div>
        {error && <p className="text-red-500 mb-4">{error}</p>}
        {signupSuccess && (
          <div className="text-green-500 mb-4">
            <p>Sign-up successful!</p>
            <p>You can now log in.</p>
          </div>
        )}
        <button
          type="submit"
          className="bg-yellow-400 text-black py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Sign Up
        </button>
      </form>
      <p className="mt-4 text-white">
        Already have an account? <Link to="/login" className="text-yellow-400">Login</Link>
      </p>
    </div>
  );
};

export default SignUp;