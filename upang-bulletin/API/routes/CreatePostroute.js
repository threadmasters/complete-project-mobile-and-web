const express = require('express');
const router = express.Router();
const Post = require('../models/CreatePostmodel');
const multer = require('multer');


const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'C:\\Users\\Lesley\\Desktop\\checking\\UPANG__BULLETIN_MAIN\\storage');
  },
  filename: function (req, file, cb) {
   
    cb(null, file.originalname);
  },
});

const upload = multer({ storage: storage });


router.post('/create', upload.array('images'), async (req, res) => {
  const {
    title,
    text,
    metaDescription,
    slugTitle,
    featured,
  } = req.body;

  console.log('Received data:', req.body);

  try {
   
    const newPost = new Post({
      title,
      text,
      images: req.files.map((file) => file.filename), 
      metaDescription,
      slugTitle,
      featured,
    });

  
    await newPost.save();

    res.status(201).json({ message: 'Post created successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
});

router.get('/all', async (req, res) => {
  try {
    const posts = await Post.find();
    res.json(posts);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Server error' });
  }
});

router.put('/:id', async (req, res) => {
  const { title, text, metaDescription, slugTitle } = req.body;

  try {
    const updatedPost = await Post.findByIdAndUpdate(
      req.params.id,
      {
        title,
        text,
        metaDescription,
        slugTitle,
      },
      { new: true }
    );

    res.status(200).json(updatedPost);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Server error' });
  }
});

router.delete('/:id', async (req, res) => {
  try {
    await Post.findByIdAndDelete(req.params.id);
    res.json({ message: 'Post deleted successfully' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Server error' });
  }
});

module.exports = router;