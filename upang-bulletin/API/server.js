require('dotenv').config();

const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const CreatePostroute = require('./routes/CreatePostroute');


const app = express();


app.use(express.json());
app.use((req, res, next) => {
  console.log(req.path, req.method);
  next();
});

app.use(cors({
  origin: 'exp://192.168.100.36:8081',
  credentials: true,
}));



mongoose.connect(process.env.MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => {
    console.log('Connected to MongoDB Atlas');
  })
  .catch((error) => {
    console.error('Error connecting to MongoDB:', error);
  });

app.use('/uploads', express.static('C:\\Users\\Lesley\\Desktop\\checking\\UPANG__BULLETIN_MAIN\\storage'));
app.use('/Post', CreatePostroute);

app.listen(process.env.PORT, () => {
  console.log('Listening on port', process.env.PORT);
});
