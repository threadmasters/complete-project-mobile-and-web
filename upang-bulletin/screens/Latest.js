import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, Modal, Linking } from 'react-native';
import COLORS from '../constants/colors';
import { FontAwesome } from '@expo/vector-icons';

const Latest = () => {
  const [featuredEvents, setFeaturedEvents] = useState([]);
  const [selectedEvent, setSelectedEvent] = useState(null);

  useEffect(() => {
    
    fetch('http://192.168.100.36:3500/Post/all') 
    .then((response) => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
        return response.json();
      })
      .then((data) => {
      
        const featuredEvents = data.filter((event) => event.featured);
        setFeaturedEvents(featuredEvents);
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  }, []);

  const renderEventItem = (item) => (
    <TouchableOpacity
      key={item._id}
      style={styles.eventItem}
      onPress={() => {
        setSelectedEvent(item);
      }}
    >
      <Image source={{ uri: `http://192.168.100.36:3500/uploads/${item.images[0]}` }} style={styles.eventImage} />
      <Text style={styles.eventTitle}>{item.title}</Text>
      <Text style={styles.eventDescription}>{item.text}</Text>
      
    </TouchableOpacity>
  );

  const closeModal = () => {
    setSelectedEvent(null);
  };

  const openLink = (url) => {
    Linking.openURL(url);
  };

  const renderSelectedEvent = () => {
    if (selectedEvent) {
      return (
        <Modal transparent={true} animationType="slide" visible={true} onRequestClose={closeModal}>
          <View style={styles.modalBackground}>
            <View style={styles.modalContainer}>
              <Text>Meta Description: {selectedEvent.metaDescription}</Text>
              <TouchableOpacity onPress={closeModal} style={styles.closeButton}>
                <FontAwesome name="times" size={20} color="black" />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.slugTitleLink}
                onPress={() => {
                 
                  openLink(selectedEvent.slugTitle); 
                }}
              >
                <Text>Slug Title: {selectedEvent.slugTitle}</Text>
              </TouchableOpacity>
              <Text>Featured: {selectedEvent.featured ? 'Yes' : 'No'}</Text>
            </View>
          </View>
        </Modal>
      );
    }
    return null;
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Upcoming Events</Text>
      <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scrollViewContent}>
        {featuredEvents.map((event) => renderEventItem(event))}
      </ScrollView>
      {renderSelectedEvent()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  header: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 16,
    color: COLORS.primary,
    marginTop: 20,
  },
  scrollViewContent: {
    marginTop: 25,
    alignItems: 'center',
  },
  eventItem: {
    width: 300,
    height: 350,
    marginBottom: 16,
    padding: 16,
    borderRadius: 10,
    backgroundColor: '#355E3B',
    borderColor: COLORS.primary,
    shadowColor: COLORS.primary,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 4,
  },
  eventImage: {
    width: 250,
    height: 200,
    borderRadius: 8,
  },
  eventTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 8,
    color: COLORS.white,
  },
  eventDescription: {
    fontSize: 16,
    color: COLORS.white,
    marginBottom: 8,
  },
  modalBackground: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContainer: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 15,
    margin: 20,
  },
  closeButton: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
  slugTitleLink: {
    padding: 2,
    color: 'blue',
    textDecorationLine: 'underline',
  },
});

export default Latest;