import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text, Image, TouchableOpacity, Modal, StyleSheet, Linking } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { SafeAreaView } from 'react-native-safe-area-context';
import COLORS from '../constants/colors';

const HomeScreen = () => {
  const [posts, setPosts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [selectedPost, setSelectedPost] = useState(null);
  const [isModalVisible, setIsModalVisible] = useState(false);

  useEffect(() => {
    fetch('http://192.168.100.36:3500/Post/all')
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => {
        setPosts(data);
        setIsLoading(false);
      })
      .catch((error) => {
        setError(error);
        setIsLoading(false);
        console.error('Error fetching data:', error);
      });
  }, []);

  const handlePostSelect = (post) => {
    setSelectedPost(post);
    setIsModalVisible(true);
  };

  const closeModal = () => {
    setIsModalVisible(false);
  };

  const openLink = (url) => {
    Linking.openURL(url);
  };

  const renderSelectedPost = () => {
    if (selectedPost) {
      return (
        <View style={styles.modalContainer}>
          <Text>Meta Description: {selectedPost.metaDescription}</Text>
          <TouchableOpacity onPress={closeModal} style={styles.closeButton}>
            <FontAwesome name="times" size={20} color="black" />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.slugTitleLink}
            onPress={() => {
              openLink(selectedPost.slugTitle);
            }}
          >
            <Text>Slug Title: {selectedPost.slugTitle}</Text>
          </TouchableOpacity>
          <Text>Featured: {selectedPost.featured ? 'Yes' : 'No'}</Text>
        </View>
      );
    }
    return null;
  };

  if (isLoading) {
    return <Text>Loading...</Text>;
  }

  if (error) {
    return <Text>Error: {error.message}</Text>;
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={{ padding: 20 }}>
        {posts.map((post) => (
          <TouchableOpacity
            key={post._id}
            style={{ borderWidth: 1, borderRadius: 15, borderColor: COLORS.primary, marginBottom: 20 }}
            onPress={() => handlePostSelect(post)}
          >
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: COLORS.primary, padding: 10 }}>{post.title}</Text>
            <View style={{ alignItems: 'center' }}>
              <Image
                source={{ uri: `http://192.168.100.36:3500/uploads/${post.images[0]}` }}
                style={{ width: '100%', height: 200 }}
              />
            </View>
            <Text style={{ padding: 10 }}>{post.text}</Text>
          </TouchableOpacity>
        ))}
        <Modal
          transparent={true}
          animationType="slide"
          visible={isModalVisible}
          onRequestClose={closeModal}
        >
          <View style={styles.modalBackground}>
            {renderSelectedPost()}
          </View>
        </Modal>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 15,
    margin: 20,
    position: 'relative',
  },
  modalBackground: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  closeButton: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
  slugTitleLink: {
    padding: 2,
    color: 'blue',
    textDecorationLine: 'underline',
  },
});

export default HomeScreen;