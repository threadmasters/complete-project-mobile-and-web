import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Modal,
  Text,
  Button,
} from "react-native";
import { Avatar, Input } from "@rneui/base";
import { auth, db } from "../config/firebase"; 
import * as ImagePicker from "expo-image-picker";
import { FontAwesome } from "@expo/vector-icons";
import COLORS from "../constants/colors";
import { updateProfile } from "firebase/auth";
import { doc, setDoc } from "firebase/firestore";

import AsyncStorage from "@react-native-async-storage/async-storage"; 

const ProfileScreen = () => {
  const user = auth.currentUser;
  const [displayName, setDisplayName] = useState("");
  const [profilePhoto, setProfilePhoto] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const [yearGradeModalVisible, setYearGradeModalVisible] = useState(false);
  const [selectedYear, setSelectedYear] = useState("");

  useEffect(() => {
    if (user) {
   
      retrieveUserProfile();
    }
  }, [user]);

  
  const retrieveUserProfile = async () => {
    try {
      const storedProfileData = await AsyncStorage.getItem("userProfile");
      if (storedProfileData) {
        const profileData = JSON.parse(storedProfileData);
        setDisplayName(profileData.displayName);
        setProfilePhoto(profileData.photoURL);
        setSelectedYear(profileData.yearGrade);
      }
    } catch (error) {
      console.error("Error retrieving user profile data from local storage:", error);
    }
  };

  const handleProfileUpdate = async () => {
    try {
      await updateProfile(user, {
        displayName,
        photoURL: profilePhoto,
      });

      const userDocRef = doc(db, "users", user.uid);
      const userData = {
        displayName,
        photoURL: profilePhoto,
        yearGrade: selectedYear,
      };
      await setDoc(userDocRef, userData, { merge: true });

      
      saveUserProfileToLocalStorage(userData);

      console.log("Profile updated and data stored in Firestore successfully");
      setIsEditing(false);
    } catch (error) {
      console.error("Error updating profile or storing data in Firestore:", error);
    }
  };

  
  const saveUserProfileToLocalStorage = async (profileData) => {
    try {
      await AsyncStorage.setItem("userProfile", JSON.stringify(profileData));
    } catch (error) {
      console.error("Error saving user profile data to local storage:", error);
    }
  };

  const pickImage = async () => {
    try {
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 0.5,
      });

      if (!result.canceled) {
        setProfilePhoto(result.assets[0].uri);
      }
    } catch (error) {
      console.error("Error picking an image:", error);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.avatarContainer}>
        <Avatar
          rounded
          size="xlarge"
          source={{
            uri:
              profilePhoto ||
              (user
                ? user.photoURL ||
                  "https://example.com/default-profile-image.jpg"
                : "https://example.com/default-profile-image.jpg"),
          }}
          onPress={() => isEditing && pickImage()}
        >
          {profilePhoto ? null : (
            <View style={styles.cameraIconContainer}>
              <FontAwesome name="user" size={90} color="#000" />
            </View>
          )}
        </Avatar>
      </View>
      <View style={styles.inputContainer}>
        <Input
          label="Display Name"
          value={displayName}
          onChangeText={setDisplayName}
          placeholder="Enter display name"
          editable={isEditing}
        />

        <TouchableOpacity onPress={() => isEditing && setYearGradeModalVisible(true)}>
          <View style={styles.pickerButton}>
            <Text>{`Year Grade: ${selectedYear}`}</Text>
          </View>
        </TouchableOpacity>
      </View>

      {!isEditing && (
        <TouchableOpacity onPress={() => setIsEditing(true)}>
          <Text style={styles.editProfileButton}>Edit Profile</Text>
        </TouchableOpacity>
      )}

      {isEditing && (
        <Button title="Update" onPress={handleProfileUpdate} />
      )}


      <Modal
        animationType="slide"
        transparent={true}
        visible={yearGradeModalVisible}
        onRequestClose={() => setYearGradeModalVisible(!yearGradeModalVisible)}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => {
                setSelectedYear("1st year");
                setYearGradeModalVisible(!yearGradeModalVisible);
              }}
            >
              <Text style={styles.modalText}>1st year</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setSelectedYear("2nd year");
                setYearGradeModalVisible(!yearGradeModalVisible);
              }}
            >
              <Text style={styles.modalText}>2nd year</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setSelectedYear("3rd year");
                setYearGradeModalVisible(!yearGradeModalVisible);
              }}
            >
              <Text style={styles.modalText}>3rd year</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setSelectedYear("4th year");
                setYearGradeModalVisible(!yearGradeModalVisible);
              }}
            >
              <Text style={styles.modalText}>4th year</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  cameraIconContainer: {
    width: 150,
    height: 150,
    borderRadius: 50,
    backgroundColor: '#d3d3d3',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 10,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  avatarContainer: {
    marginBottom: 20,
  },
  inputContainer: {
    width: '100%',
  },
  outputContainer: {
    marginTop: 20,
  },
  editProfileButton: {
    color: 'white',
    backgroundColor: COLORS.primary,
    fontSize: 22,
    marginTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 10,
    paddingTop: 10,
    borderRadius: 10,
  },
  pickerButton: {
    backgroundColor: '#f0f0f0',
    padding: 10,
    borderRadius: 5,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 35,
    alignItems: 'center',
    elevation: 5,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

export default ProfileScreen;